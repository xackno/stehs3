@extends('layouts.app')

@section('content')

    <div class="content">
        <div class="panel-header bg-primary-gradient">
          <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
              <div>
                <h2 class="text-white pb-2 fw-bold">INICIO</h2>
                <h5 class="text-white op-7 mb-2">Stehs LATAM</h5>
              </div>
              <div class="ml-md-auto py-2 py-md-0">
                <a href="http://stehs.com" class="btn btn-white btn-border btn-round mr-2" target="_blank">Página</a>
                <a href="#" class="btn btn-secondary btn-round">Add Customer</a>
              </div>
            </div>
          </div>
    </div>

@endsection
