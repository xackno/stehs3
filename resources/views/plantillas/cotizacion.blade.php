<!DOCTYPE html>
<html>
<head>
	<title>cotizacion</title>
	  <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}">
	  <link rel="stylesheet" href="{{asset('/css/atlantis.min.css')}}">
	  <link rel="stylesheet" href=" {{asset('/css/demo.css')}}">
</head>
<body class="text-center">
	<img src="{{asset('img/headerMov.png')}}" style="width: 80%">

	<b>Fecha:{{date('d-m-Y')}}</b>

	<table class=" table-bordered table-striped">
		<thead class="table-primary">
			<tr>
				<th>Cantidad</th>
				<th>unidad</th>
				<th>Descripcion</th>
				<th>precio</th>
				<th>subtotal</th>
			</tr>
		</thead>
		<tbody>

			@for($x=0;$x < count($id_p); $x++)
			<tr>
				<td>{{$cantidad[$x]}}</td>
				<td>{{$unidad[$x]}}</td>
				<td>{{$descripcion[$x]}}</td>
				<td>${{$precio[$x]}}</td>
				<td>${{$subtotal[$x]}}</td>
			</tr>
			@endfor
		</tbody>
	</table>
	<h2 class="float-right"><b>TOTAL:${{number_format(array_sum($subtotal),2)}}</b> MXN</h2>
	<br>
	<br>
	<p><b>Nota:</b> Los precios son puesto en la obra Vigencia de la Cotización 15 días a partir de la fecha de emisión</p>
	<style type="text/css">
		table{
			width: 100%
		}
		table td{
			padding: 3px;
		}table th{padding: 3px}

	</style>

	<div class="footer" style="position: absolute;bottom: 0px">
		<h3>Datos para pagos:</h3>
		<table id="table_fotter">
			<thead>
				<tr>
					<th>BANCO</th>
					<th>SUCURSAL</th>
					<th>CUENTA</th>
					<th>CLAVE INTERVANCARIA</th>
					<th>NOMBRE</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Banamex</td>
					<td>7013</td>
					<td>7149360</td>
					<td>002613701371493603</td>
					<td>Mendoza Cuevas Eva Eloisa</td>
				</tr>
				<tr>
					<td>BANORTE</td>
					<td></td>
					<td>0333295946</td>
					<td>072613003332959461</td>
					<td>Mendoza Cuevas Eva Eloisa</td>
				</tr>
			</tbody>
		</table>
	</div>

</body>
</html>


