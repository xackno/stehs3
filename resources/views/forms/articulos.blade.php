@extends('layouts.app')

@section('content')

<div class="card" style="margin-top: 60px;">
	<div class="card-header bg-success" style="width: 100%">
		<div class="row">
			<div class="col">
				<h2 class="text-white float-left">Articulos <i class="fas fa-box-open"></i></h2>
			</div>
			<div class="col"></div>
			<div class="col"></div>
			<div class="col">
				<div class="input-group">
					<button class="btn btn-sm btn-primary" id="btn_open_modal_producto"> <i class="fas fa-plus"></i> </button>
					<button class="btn btn-sm btn-secondary" id="btn_open_modal_marca"> <i class="fas fa-plus"></i> Marca </button>
					<button class="btn btn-sm btn-warning" id="btn_open_modal_provedor"> <i class="fas fa-plus"></i>  Provedor</button>
					@if(Auth::User()->type=='administrador')
					<button class="btn btn-sm btn-danger" id="btn_open_existencia"> <i class="fas fa-plus"></i> Existencia</button>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div id="alertas">
		</div>
		<div class="table-responsive">
			@if(Request::path()!='articulos')
				<a href="{{url('/articulos')}}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i> Todos</a>
				<br><br>
			@endif
			<table class="table table-bordered table-striped">
				<thead class="table-primary">
					<tr class="text-center">
						<th>Codigo</th>
						<th>Clave</th>
						<th>Unidad</th>
						<th>Descripción</th>
						<th><i class="fas fa-barcode fa-2x"></i></th>
						@if(Auth::User()->type=='administrador')
						<th>$ compra</th>
						@endif
						<th>$ venta</th>
						<th>Cant</th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
				<tbody>
					@foreach($articulos as $articulo)
						<tr>
							<td>{{$articulo->codigo}}</td>
							<td>{{$articulo->clave}}</td>
							<td>{{$articulo->unidad}}</td>
							<td>
								<b class="text-primary">{{$articulo->descripcion}}</b>
							</td>
							<td class="text-center">{{$articulo->cod_barra}}</td>
							@if(Auth::User()->type=='administrador')
								<td class="text-right">${{$articulo->compra}}</td>
							@endif
							<td class="text-right"><b class="text-sucess">${{$articulo->venta}}</b> </td>
							<td class="text-right"><b class="text-danger">{{$articulo->cantidad}}</b> </td>
							<td>
								<button class="btn btn-info btn-sm "  onclick="editar({{$articulo->id}});"  @if(Auth::user()->type!='administrador') disabled='' @endif>
									<i class="fas fa-edit "></i></button>

								<button class="btn btn-danger btn-sm " onclick="eliminar({{$articulo->id}});" @if(Auth::user()->type!='administrador') disabled='' @endif>
									<i class="fas fa-trash fa-sm"></i>
								</button>

							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			@if(Request::path()=='articulos')
				{{$articulos->links()}}
			@endif
		</div>
		<style type="text/css">
			.table th{
				/*padding: 50px;*/
				height: 40px;
			}
		</style>
	</div>
</div>

<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_existencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Existencia</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="alertasModal"></div>

        	<div class="input-group col-4">
        		<input type="text" id="input_buscar" class="form-control" placeholder="Buscar" autocomplete="off">
        		<button  id="btn_buscar_para_existencia" class="btn btn-sm btn-primary"><i class="fas fa-search"></i></button>
        	</div>
        	<br>
        	<div class="table-responsive" >
        		<table class="table table-striped table-bordered">
        			<thead class="table-success">
        				<tr>
        					<th>Entradas</th>
        					<th>Existencia</th>
        					<th>Codigo</th>
        					<th >Descripción</th>
        				</tr>
        			</thead>
        			<tbody id="tbody_existencia"></tbody>
        		</table>
        	</div>

        	<button class="btn btn-primary float-right" id="guardar_cambios"><i class="fas fa-save"></i> Guardar cambios</button>

        </div>
      </div>
    </div>
  </div>


<!--window modal ######modal marca################-->
  <div class="modal fullscreen-modal fade" id="modal_agregar_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Marca</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form id="form_marca">
        		<label>Nombre</label>
	        	<input type="text" name="nombre" class="form-control" placeholder="Nombre">

	        	<label>Descripción</label>
	        	<input type="text" name="descripcion" class="form-control" placeholder="Descripción">

	        	<label>Provedor</label>
	        	<select class="form-control" name="provedor">
	        		<option disabled="" selected="">Secciones una opción</option>
	        		@foreach($provedores as $provedor)
	        			<option value="{{$provedor->id}}">{{$provedor->nombre}}</option>
	        		@endforeach
	        	</select>

	        	<label>Logotipo</label>
	        	<input type="" name="logo" class="form-control" disabled="">
	        	<!-- <input type="file" name="logo" placeholder="logo" class="form-control" > -->
	        	<br><br>
	        	<button type="button" id="btn_store_marca" class="btn btn-success float-right"><i class="fas fa-save"></i> Guardar</button>
        	</form>

        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal provedor################-->
  <div class="modal fullscreen-modal fade" id="modal_agregar_provedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-warning">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Provedor</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form id="form_provedor">
        		<div class="row">
        			<div class="col">
        				<label>Nombre</label>
			        	<input type="text" name="nombre" class="form-control" placeholder="Nombre">

			        	<label>Descripción</label>
			        	<input type="text" name="descripcion" class="form-control" placeholder="Descripción">

			        	<label>RFC</label>
			        	<input type="text" name="rfc" class="form-control">
        			</div>
        			<div class="col">
        				<label>Domicilio</label>
			        	<input type="text" name="domicilio" class="form-control">

			        	<label>Telefono</label>
			        	<input type="number" name="tel" class="form-control">

			        	<label>Email</label>
			        	<input type="email" name="email" class="form-control">
        			</div>
        		</div>

	        	<br><br>
	        	<button type="button" id="btn_store_provedor" class="btn btn-success float-right"><i class="fas fa-save"></i> Guardar</button>
        	</form>

        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal agregar productos################-->
  <div class="modal fullscreen-modal fade" id="modalagregarproductos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus" id="icon_header"></i> Articulo</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form   id="form_articulo">
        		<div class="row">
        		<div class="col">
        			<input type="hidden" name="id_p">
        			<label>Código</label>
        			<input type="text" name="codigo" class="form-control" placeholder="Codigo">

        			<label>Clave</label>
        			<input type="text" name="clave" class="form-control" placeholder="Clave">

        			<label>Unidad</label>
        			<select class="form-control" name="unidad" required="">
        				<option selected="" disabled="">Seleccione uno</option>
        				<option value="pieza">Pieza</option>
        				<option value="kilogramo">Kilogramo</option>
        				<option value="litro">Litro</option>
        				<option value="gramos">Gramos</option>
        				<option value="bolsa">bolsa</option>
        			</select>
        			<label>Descripción</label>
        			<input type="text" name="descripcion" class="form-control border-success" placeholder="descripción" required="">
        		</div>
        		<div class="col">
        			<label>Código de barra</label>
        			<input type="number" name="cod_barra" class="form-control">

        			<label>Precio de compra</label>
        			<input type="number" name="compra" class="form-control" placeholder="precio de compra">

        			<label>Precio de venta</label>
        			<input type="number" name="venta" class="form-control border-success" placeholder="precio de venta" required="">

        			<label>Cantidad</label>
        			<input type="number" name="cantidad" class="form-control" placeholder="cantidad en existencia">
        		</div>
        		<div class="col">
        			<label>Código SAT</label>
        			<input type="text" name="codigo_sat" class="form-control">

        			<label>Descripción SAT</label>
        			<input type="text" name="desc_sat" class="form-control">

        			<label>Marca</label>
        			<select name="marca" class="form-control">
        				<option selected="" disabled="">Seleccione uno</option>
        				@foreach($marcas as $marca)
        					<option value="{{$marca->id}}">{{$marca->nombre}}</option>
        				@endforeach
        			</select>
        		</div>
        	</div>
        	<br><br>
        	<button type="button" class="btn btn-success float-right" id="btn_submit_actualizar" style="display: none">Actualizar</button>
        	<button type="button" id="btn_submit_articulo" class="btn btn-primary float-right"><i class="fas fa-save"></i> Guardar</button>
        	</form>



        </div>
      </div>
    </div>
  </div>



@endsection
@section('script')
<script type="text/javascript">
	//############btn-actualizar#################
		$("#btn_submit_actualizar").click(function(){

			$.ajax({
				url:"{{url('update_articulo')}}",
				type:"post",
				dataType:"json",
				data:$("#form_articulo").serialize(),
				success:function(e){

					$("#form_articulo")[0].reset();
					$("#alertas").append('<div class="alert alert-success">Articulo actualizado correctamente.</div>');

					setInterval(function(){
						location.reload();
					},3000);
					$("#modalagregarproductos").modal('hide');
				},
				error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al actualizar el articulo.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},3000);
					$("#modalagregarproductos").modal('hide');
				}
			});
		});

	//###################editar#################
		function editar(id){
			$("#form_articulo")[0].reset();
			$("#btn_submit_articulo").hide();
			$("#btn_submit_actualizar").show();
			$("#modalagregarproductos").modal("show");
			$("#icon_header").removeClass();
			$("#icon_header").addClass("fas fa-edit");
			$.ajax({
				url:"{{route('buscar_para_editar')}}",
				type:"post",
				dataType:"json",
				data:{id:id},success:function(e){
					$("[name='codigo']").val(e.codigo);
					$("[name='clave']").val(e.clave);
					$("[name='unidad']").val(e.unidad);
					$("[name='descripcion']").val(e.descripcion);
					$("[name='cod_barra']").val(e.cod_barra);
					$("[name='compra']").val(e.compra);
					$("[name='venta']").val(e.venta);
					$("[name='cantidad']").val(e.cantidad);
					$("[name='codigo_sat']").val(e.codigo_sat);
					$("[name='desc_sat']").val(e.desc_sat);
					$("[name='marca']").val(e.marca);
					$("[name='provedor']").val(e.provedor);
					$("[name='id_p']").val(e.id);




				},error:function(e){
					alert("no se encontro!");
				}
			});

		}
	//######################eliminar articulo#################
		function eliminar(id){
			var msj=confirm("Desea eliminar este articulo?");
			if (msj) {
				$.ajax({
					url:"{{route('eliminarArticulo')}}",
					type:"post",
					data:{
						id:id
					},success:function(){
						$("#alertas").append("<div class='alert alert-warning'>Eliminado corectamente</div>");
						setInterval(function(){
							location.reload();
						},2000);
					},error:function(){
						$("#alertas").append("<div class='alert alert-danger'>Error al eliminar este articulo.</div>");
						setInterval(function(){
							$("#alertas").html('');
						},2000);
					}
				});
			}
		}




	//##########################existencia###########
		$("#btn_open_existencia").click(function(){
			$("#modal_existencia").modal("show");
		});
		$("#input_buscar").on("keyup",function(e){
			if (e.keyCode==13) {
				buscarproductos();
				$(this).val('');
			}
		});
		$("#guardar_cambios").hide();

		function buscarproductos(){
			$("#tbody_existencia").html('');
			$.ajax({
				url:"{{route('buscarExistencia')}}",
				type:"post",
				dataType:"json",
				data:{
					buscar:$("#input_buscar").val()
				},success:function(e){
					for(var x=0;x<e.length;x++){
						$("#tbody_existencia").append("<tr> <td style='width:20%'> <input type='number' class='form-control' style='width: 100%'></td><td>"+e[x].cantidad+"</td><td>"+e[x].codigo+" </td><td>"+e[x].descripcion+" </td><td class='d-none'>"+e[x].id+"</td></tr>");
					}
					$("#guardar_cambios").show();
				}
			});
		}
		$("#btn_buscar_para_existencia").click(function(){
			buscarproductos();
			$("#input_buscar").val('');
		});
		//---------------------------------------------
		$("#guardar_cambios").click(function(){
			var id=[]; var cantidad=[];
			$("#tbody_existencia").find("tr td:first-child").each(function(){
				// alert($(this).find('input').val());
				if(parseFloat($(this).find('input').val())){
					cantidad.push($(this).find('input').val());
					id.push($(this).siblings("td").eq(3).html());
				}
          });//fin each
			$.ajax({
				url:"{{route('updateExistenca')}}",
				type:"post",
				dataType:"json",
				data:{
					id:id,
					cantidad:cantidad
				},success:function(e){
					$("#tbody_existencia").html('');
					if (e=="success") {
						$("#alertasModal").append("<div class='alert alert-success'>Modificado correctamente.</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);
					}
				},error:function(){

						$("#alertasModal").append("<div class='alert alert-danger'>Error al modificar, verifique la información proporcionada...</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);

				}
			});
		});

	//##################submit provedor###############
		$("#btn_open_modal_provedor").click(function(){
			$("#modal_agregar_provedor").modal('show');
		});

		$("#btn_store_provedor").click(function(){
			$.ajax({
				url:"{{route('store_provedor')}}",
				type:"post",
				dataType:"json",
				data:$("#form_provedor").serialize(),
				success:function(e){
					$("#form_provedor")[0].reset();
						$("#alertas").append('<div class="alert alert-success">Provedor agregado correctamente.</div>');
						setInterval(function(){
							$("#alertas").html('');
							location.reload();
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				},error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar, verifique la información proporcionada</div>');
						setInterval(function(){
							$("#alertas").html('');
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				}
			});
		});






    //################submit marca#################
		$("#btn_open_modal_marca").click(function(){
			$("#modal_agregar_marca").modal('show');
		});

		$("#btn_store_marca").click(function(){
			$.ajax({
				url:"{{route('store_marca')}}",
				type:"post",
				dataType:"json",
				data:$("#form_marca").serialize(),
				success:function(e){
					$("#form_marca")[0].reset();
						$("#alertas").append('<div class="alert alert-success">Marca agregada correctamente.</div>');
						setInterval(function(){
							$("#alertas").html('');
							location.reload();
						},3000);
						$("#modal_agregar_marca").modal('hide');
				},error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar, verifique la información proporcionada</div>');
						setInterval(function(){
							$("#alertas").html('');
						},3000);
						$("#modal_agregar_marca").modal('hide');
				}
			});
		});

	//#####################submit articulos#################################
		$("#btn_open_modal_producto").click(function(){
			$("#modalagregarproductos").modal('show');
			$("#btn_submit_actualizar").hide();
			$("#btn_submit_articulo").show();
			$("#form_articulo")[0].reset();
			$("#icon_header").removeClass();
			$("#icon_header").addClass("fas fa-plus");
		});
		$("#btn_submit_articulo").click(function(){
			$.ajax({
				url:"{{route('store_articulo')}}",
				type:"post",
				dataType:"json",
				data:$("#form_articulo").serialize(),
				success:function(e){
					$("#form_articulo")[0].reset();
					$("#alertas").append('<div class="alert alert-success">Articulo agregado correctamente.</div>');
					setInterval(function(){
						$("#alertas").html('');
						location.reload();
					},3000);
					$("#modalagregarproductos").modal('hide');
				},
				error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar el articulo.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},3000);
					$("#modalagregarproductos").modal('hide');
				}
			});
		});


</script>
@endsection
