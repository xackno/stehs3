@extends('layouts.app')

@section('content')
<!-- ############################################################### -->
    <div class="card" style="margin-left: auto" id="card_contenido_ventas">
      <div class="card-body" style="padding: 0px">
        <div class="table-responsive" style="overflow-y: scroll;height: 85vh; position: absolute;">
          <table class="table table-striped table-bordered">
            <thead class="table-danger text-center">
              <tr>
                <th scope="col" class="d-none">#</th>
                <th scope="col">CANTIDAD</th>
                <th scope="col">UNIDAD</th>
                <th scope="col">ARTICULO</th>
                <th scope="col">PRECIO</th>
                <th scope="col">SUBTOTAL</th>
                <th scope="col"><i class="fas fa-cog"></i></th>
              </tr>
            </thead>
            <tbody class=" text-center" id="tabla_venta">
              <tr  class="d-none" >
                        <td class="d-non" ></td>
                        <td id="td_focus"></td>
                        <td></td>

                        <td></td>


                      </tr>

            </tbody>
          </table>
        </div>

      </div>
    </div>
     <style type="text/css">
        .table  th {
          padding:0px;
          height: 32px;
        }.table  td {
          padding:0px;
          height: 32px;
          font-family: 'arial black';
        }
        #tabla_venta td:nth-child(2){
          color:red;
          font-size: 150%;
        }
        #btn_funciones button{padding: 2px;}
     </style>

    <div class="card bg-primary" style="bottom:-29.8px;position: absolute;width: 100%">
      <div class="card-body" style="padding: 0px;margin-top:2px;margin-bottom: 2px;margin-right: 15px">
        <div class="row">
          <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
            <div class="input-group">
              <input type="text" name="busqueda"  class="form-control" placeholder="Buscar..." autocomplete="off" id="findProducto" autofocus="true" style="color:black;font-family: 'arial black'">

              <button class="btn btn-sm btn-success" onclick="buscarPro();"><i class="fas fa-search" ></i></button>
            </div>
            <p class="text-white" style="margin:0px;padding: 0px;border: 0px">
                <span class="text-warning">{{$fecha}} -<span id="hora_footer"></span></span>
                <span>Atiende: {{Auth::user()->name}}</span>
                @if(isset($turno[0]))
                  <span>Turno: {{$turno[0]->id}}</span>
                  <input type="hidden" name="turno" id="id_turno" value="{{$turno[0]->id}}">
                  @else
                  <span class="text-warning" style="font-family: 'Arial Black'">Iniciar TURNO</span>
                @endif
              </p>
          </div>
          <div class="col-3 col-sm-4 col-lg-4 col-xl-4" style="font-family: 'arial black'" id="btn_funciones">
             <button class="btn btn-sm btn-warning" id="btn_f9"><i class="fas fa-shopping-basket text-dark fa-2x"></i></button>
             <button class="btn btn-sm btn-warning" id="btn_f8"><i class="fas fa-power-off text-dark fa-2x"></i></button>
             <button class="btn btn-sm btn-warning" id="btn_f4"><i class="fas fa-sign-out-alt fa-2x text-dark"></i></button>
             <button class="btn btn-sm btn-warning"><i class="text-white">DEV</i></button>
             <div id="alertas"></div>
          </div>
          <div class="col-9 col-sm-4 col-lg-4 col-xl-4">
             <h1 class="text-white float-right"><span>TOTAL:$</span> <span id="totalpagar">00.00</span> MXN</h1>
          </div>
        </div>

      </div>
    </div>

<!-- ######################################### -->





<!-- #################################modales###################################3 -->
<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_busqueda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header bg-info">
          <h4 id="nivel_cliente"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="table_responsive" style="overflow-x: scroll;overflow-y: scroll;height: 400px">
          <table id="tabla_busqueda" editabled="" class="table table-striped table-bordered" >
               <thead class="table-dark">
                   <tr>
                      <th class="d-none">Id</th>
                      <th>Clave</th>
                      <th >Unidad</th>
                      <th style="width: 50%">Descripción</th>
                      <th>$ Precio</th>
                      <th>Exist</th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" >
                  <tr  class="d-none" >
                    <td></td>
                    <td></td>
                    <td></td>
                    <td id="start0"></td>
                  </tr>
               </tbody>
           </table>
        </div>
        </div>
      </div>
    </div>
  </div>

<!-- $$$$$$$$$$$$$$$modal cobrar###################### -->
  <div class="modal fade" id="modal_cobrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding: 3px">
          <h3 class="text-white">
            <i class="fas fa-shopping-cart"></i>
             Realizar venta
           </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col text-center">
              <h1 class="text-success">TOTAL:$</h1>
              <h1 id="text-total" class="text-danger">00.00</h1>
            </div>
            <div class="col text-center">
              EFECTIVO:<i class="fas fa-dollar-sign"></i>
              <input type="number" id="efectivo" class="form-control border border-primary">
            </div>
          </div><br>
          <div class="text-center">
            <h3 class="text-primary">Cambio:$<span id="cambio" class="text-danger">00.00</span></h3>
          </div>

        </div>
        <div class="card-footer">
          <button class="btn btn-warning btn-sm float-left"><i class="fas fa-credit-card"></i> Credito</button>

          <form  action="{{url('/cotizacion')}}" class="float-left" id="form_cotizacion" target="_blank" method="get" >
            <input type="hidden" name="id_p">
            <input type="hidden" name="cantidad">
            <input type="hidden" name="unidad">
            <input type="hidden" name="descripcion">
            <input type="hidden" name="precio">
            <input type="hidden" name="subtotal">
            <button class="btn btn-dark btn-sm float-left" id="btn_cotizacion"><i class="fas fa-table"></i> Cotización</button>
          </form>

          <button class="btn btn-primary btn-sm float-right" onclick="ejecutar_venta();">Realizar <i class=" fas fa-angle-right"></i></button>

        </div>
      </div>
    </div>
  </div>
<!-- fin modal cobrar -->



<!--window modal ######modal turno################-->
  <div class="modal fullscreen-modal fade" id="modal_turno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h3 class="text-white" style="margin:0px" id="header_turno">Turno</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <!-- #############################from para abrir turno -->
          <form action="{{url('/storeturno')}}" method="post" id="form-store-turno">
            @csrf
            <h4 style="width: 100%">Usuario: {{Auth::user()->name}}<span class="float-right">{{$fecha}}</span> </h4>

            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <input type="hidden" name="status" value="abierto">
            <label><i class="fas fa-dollar-sign"></i> En caja</label>
            <input type="number" name="inicio" class="form-control border-secondary" required="" placeholder="0.00">

            <label><i class="fas fa-comment-dots"></i> Comentario</label>
            <textarea class="form-control border-secondary" name="comentario1" ></textarea>
          </form>
          <!-- #####################from para cerrar turno -->
          <form action="{{url('/cerrarturno')}}" method="post" id="form-cerrar-turno">
            @csrf
            <h4>Usuario: {{Auth::user()->name}}</h4>
            <input type="hidden" name="id" @if(isset($turno[0])) value="{{$turno[0]->id}}" @endif >
            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <input type="hidden" name="status" value="cerrado">
            <label><i class="fas fa-dollar-sign"></i> En caja</label>
            <input type="number" name="cierre" class="form-control border-secondary" required="" placeholder="0.00">

            <label><i class="fas fa-comment-dots"></i> Comentario</label>
            <textarea class="form-control border-secondary" name="comentario2" ></textarea>
          </form>
        </div>
        <div class="modal-footer">
          <button id="btn_submit_cerrar_turno" class="btn btn-danger btn-sm float-right">Cerrar <i class="fas fa-power-off"></i></button>
          <button id="btn_submit_store_turno" class="btn btn-primary btn-sm float-right">Iniciar <i class="fas fa-power-off"></i></button>
        </div>

      </div>
    </div>
  </div>
<!--window modal ######modal salidas################-->
  <div class="modal fullscreen-modal fade" id="modal_salidas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h3 class="text-white" style="margin:0px">SALIDAS</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div id="alertasmodal_salidas"></div>
          <form id="form_save_salidas">
            <div class="row">
              <div class="col">
                <h4 style="width: 100%;padding:0;margin: 0px">Usuario: {{Auth::user()->name}}</h4>
              </div>

              <div class="col">
                @if(isset($turno[0]))
                  <span class="text-danger" style="font-family: 'Arial Black';padding: 0">Turno: {{$turno[0]->id}}</span>
                  @else
                  <span class="text-warning" style="font-family: 'Arial Black'">Iniciar TURNO</span>
                @endif
              </div>

              <div class="col">
                <h4><span class="float-right">{{$fecha}}</span></h4>
              </div>
            </div>
            <input type="hidden" name="turno" @if(isset($turno[0])) value="{{$turno[0]->id}}" @endif >
            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <label>Concepto</label>
            <input type="text" name="concepto" class="form-control border-secondary" required="">

            <label><i class="fas fa-dollar-sign"></i> Cantidad</label>
            <input type="number" name="cantidad" class="form-control border-secondary" placeholder="0.00">
            </form>
        </div>
        <div class="modal-footer">
          <button onclick="registrar_salidas();" class="btn btn-primary btn-sm float-right">Registrar</button>
        </div>

      </div>
    </div>
  </div>












@endsection
@section('script')
<script type="text/javascript" src="{{asset('/js/for-ventas.js')}}"></script>
<script type="text/javascript">


//###############registrar salidas##########
function registrar_salidas(){


  $.ajax({
    url:"{{url('/storesalida')}}",
    type:"post",
    dataType:"json",
    data:$("#form_save_salidas").serialize(),
    success:function(e){
      $("#alertasmodal_salidas").html("<div class='bg-success text-dark '>"+e+"</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
    },error:function(){
      $("#alertasmodal_salidas").html("<div class='bg-danger text-white '>ERROR AL GUARDAR ESTE REGISTRO</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
    }
  });
}



//###########################ejecutar venta##########################

function ejecutar_venta(){
  var id_p=[];
  var cantidad=[];
  var unidad=[];
  var descripcion=[];
  var precio=[];
  var subtotal=[];
  var cont=0;
  $("#tabla_venta").find("tr td:first-child").each(function(){
    if (cont>0) {
      id_p.push($(this).html());
      cantidad.push($(this).siblings("td").eq(0).html());
      unidad.push($(this).siblings("td").eq(1).html());
      descripcion.push($(this).siblings("td").eq(2).html());
      precio.push($(this).siblings("td").eq(3).html());
      subtotal.push($(this).siblings("td").eq(4).html());
    }
    cont++;
  });
  //-------------------------------------------------------------
  if (id_p.length!=0) {//si existe productos en la tabla de venta

    $.ajax({
      url:"{{url('ejecutar_venta')}}",
      type:"post",
      dataType:"json",
      data:{
        id_p:id_p,
        cantidad:cantidad,
        unidad:unidad,
        descripcion:descripcion,
        precio:precio,
        subtotal:subtotal,
        efectivo:$("#efectivo").val(),
        user:"{{Auth::user()->name}}",
        id_turno:$("#id_turno").val()
      },success:function(e){
        // alert(e);
        $("#alertas").html("<div class='bg-success text-white rounded'>Venta correta</div>");
        setInterval(function(){
          $("#alertas").html('');
        },3000);
        $("#modal_cobrar").modal("hide");
        $("#efectivo").val('');
        limpiartablaventa();

      },error:function(){
        alert("No se pudo realizar la venta.");
      }
    });
  }else{//si no hay producto en la tabla de venta
    alert("No hay producto en la tabla de venta.");
  }

}


//####################funcion buscar por codigo de barras#############
    function buscar_x_codigo(codigo){

      $.ajax({
        url:"{{url('/buscar_x_codigo')}}",
        type:"post",
        dataType:"json",
        data:{codigo:codigo},
        success:function(e){
          if (ya_existe(e[0].id)==0) {
              $("#tabla_venta").append("<tr tabindex='0' class='move'>  <td class='d-none'>"+e[0].id+"</td> <td contenteditable=''>1</td> <td>"+e[0].unidad+"</td> <td class='text-primary'>"+e[0].descripcion+"</td> <td>"+e[0].venta+"</td> <td>"+e[0].venta+"</td> <td onclick='eliminarPRO();'><i class='fas fa-trash text-danger'></i></td> </tr>");
            $("#findProducto").val('');
            $("#findProducto").focus();
            calcularTotal();
            }
            $("#findProducto").val('');
        },error:function(){
          alert("No se encontro el producto");
        }
      });
    }

//################FUNCION DE BUSCAR PRODUCTO############################
  function buscarPro(){
    limpiartablabusqueda();
    $('#modal_busqueda').modal('show');
    var producto=$("#findProducto").val();
        $.ajax({
          url:'{{route("buscarExistencia")}}',
          type:'post',
          dataType:'json',
          data:{
            buscar:producto
          },success:function(e){
             if (e=="") {
               $("#tabla_busqueda tbody").append("<tr  class='text-danger'> <td  colspan='6'><b>No se encontró registro!!</b></td></tr>");
              }else{
                for(var x=0;x<e.length;x++){
                $("#tabla_busqueda tbody").append("<tr tabindex='0' class='move'><td width='1px' class='d-none'>"+e[x].id+
                  "</td>   <td>"+e[x].clave+
                  "</td>   <td>"+e[x].unidad+
                  "</td>   <td>"+e[x].descripcion+
                  "</td>   <td>"+e[x].venta+
                  "</td>   <td>"+e[x].cantidad+
                  "</td>  </tr>");
               }//fin for
              }
          },error:function(){
            alert("Hubo un problema al buscar este producto");
          }
            });
            $("#tabla_busqueda tbody tr[tabindex=0]").focus();
  }



</script>
@endsection
