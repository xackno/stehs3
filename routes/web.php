<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    // return view('welcome');
    return view('auth.login');
});

// Auth::routes();
Route::get('login', "Auth\LoginController@showLoginForm")->name("login");
Route::post("/logout", "Auth\LoginController@logout")->name("logout");
Route::post("/login", "Auth\LoginController@login");

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth', 'verified'])->group(function () {

    //para salidas
    Route::post("/storesalida", "generalController@storesalidas");
    //para turnos
    Route::post("/storeturno", "generalController@storeturno");
    Route::post("/cerrarturno", "generalController@cerrarturno");

    //registrar usuarios
    Route::get("/registrar", "generalController@registrar"); //para mostrar la vista de regsitrar
    Route::post("register", "Auth\RegisterController@register")->name("register"); //ejecutar form

    //para venta
    Route::get("/ventas", "ventasController@index");
    Route::post("/ejecutar_venta", "ventasController@ejecutar_venta");
    Route::get("/cotizacion", "ventasController@cotizacion");

    //para articulos##############
    Route::get("/articulos", "articulosController@index");
    Route::post("articulo", "articulosController@store")->name('store_articulo');
    Route::get("buscarArticulo", "articulosController@buscarArticulos")->name('buscarArtiulos');
    Route::post("/buscar_x_codigo", "articulosController@buscar_x_codigo");

    Route::post("buscarExistencia", "articulosController@buscarExistencia")->name('buscarExistencia');
    Route::post("updateExistenca", "articulosController@updateExistenca")->name('updateExistenca');
    Route::post("eliminarArticulo", "articulosController@eliminarArticulo")->name("eliminarArticulo");
    Route::post("buscar_para_editar", "articulosController@buscar_para_editar")->name("buscar_para_editar");
    Route::post("update_articulo", "articulosController@update_articulo");

    Route::post("marcas", "articulosController@store_marca")->name("store_marca");
    Route::post("provedor", "articulosController@store_provedor")->name("store_provedor");
    //################clientes##############
    Route::get("/clientes", "clientesController@index");
    Route::post("cliente", "clientesController@store")->name("storeCliente");
    Route::post("destroycliente", "clientesController@destroy")->name("destroyCliente");
    Route::post("showCliente", "clientesController@showCliente")->name("showCliente");
    Route::post("/updateCliente", "clientesController@update");

});
