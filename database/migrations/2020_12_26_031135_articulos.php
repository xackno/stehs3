<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Articulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->nullable();
            $table->string('clave')->nullable();
            $table->string('unidad');
            $table->string('descripcion');
            $table->bigInteger('cod_barra')->nullable();
            $table->decimal('compra',10,2);
            $table->decimal('venta',10,2);
            $table->string('codigo_sat')->nullable();
            $table->string('desc_sat')->nullable();
            $table->integer('marca')->default(0);
            $table->decimal('cantidad',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
