<?php

namespace App\Http\Controllers;

use App\models\turnos;
use App\Models\ventas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ventasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dia   = date("N");
        $mes   = date("m");
        $dias  = ["Lun", "Mar", "Mie", "Juv", "Vie", "Sab", "Dom"];
        $meses = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
        // $fecha = $dias[$dia - 1] . "  " . date("d") . " " . $meses[$mes - 1] . " de " . date("Y");
        $fecha = date("d") . " " . $meses[$mes - 1] . " de " . date("Y");
        $turno = turnos::where("usuario", "=", Auth::user()->id)
            ->where("status", "=", "abierto")
            ->get();
        return view('forms.ventas', compact("turno", "fecha"));
    }
    public function ejecutar_venta(Request $data)
    {

        $id_p        = $data->get("id_p");
        $cantidad    = $data->get("cantidad");
        $unidad      = $data->get("unidad");
        $descripcion = $data->get("descripcion");
        $precio      = $data->get("precio");
        $subtotal    = $data->get("subtotal");
        $efectivo    = $data->get("efectivo");
        $user        = $data->get("user");
        $id_turno    = $data->get("id_turno");

        try {
            $venta = ventas::create([
                "usuario"      => $user,
                "id_turno"     => $id_turno,
                "efectivo"     => $efectivo,
                "total_venta"  => array_sum($subtotal),
                "cantidad_pro" => implode(",", $cantidad),
                "id_productos" => implode(",", $id_p),
            ]);
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);

    }

    public function cotizacion(Request $data)
    {

        $id_p        = $data->get("id_p");
        $cantidad    = $data->get("cantidad");
        $unidad      = $data->get("unidad");
        $descripcion = $data->get("descripcion");
        $precio      = $data->get("precio");
        $subtotal    = $data->get("subtotal");

        $id_p        = explode(",", $id_p);
        $cantidad    = explode(",", $cantidad);
        $unidad      = explode(",", $unidad);
        $precio      = explode(",", $precio);
        $subtotal    = explode(",", $subtotal);
        $descripcion = explode("=>", $descripcion);

        // return view('plantillas.cotizacion', compact('id_p','cantidad','unidad','descripcion','precio','subtotal'));

        $view = \View::make('plantillas.cotizacion', compact('id_p', 'cantidad', 'unidad', 'descripcion', 'precio', 'subtotal'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('Formato salida-' . date('d-m-Y'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
