<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\articulos;
use App\Models\marcas;
use App\Models\provedores;
class articulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articulos=articulos::paginate(8);
        $marcas=marcas::all();
        $provedores=provedores::all();
        return view("forms.articulos",compact('articulos','marcas','provedores'));
    }
    public function buscarExistencia(Request $data)
    {
        $buscar=$data->get('buscar');
        $articulos=articulos::orWhere("descripcion","LIKE","%$buscar%")
        ->orWhere("codigo","=","$buscar")
        ->orWhere("clave","=","%$buscar%")
        ->orWhere("descripcion","=","%$buscar%")
        ->orWhere("cod_barra","=","$buscar")
        ->get();
        return json_encode($articulos);
    }

    public function buscar_x_codigo(Request $data){
        $buscar=$data->get('codigo');
        $articulos=articulos::orWhere("cod_barra","=","$buscar")
        ->get();
        return json_encode($articulos);
    }

    public function buscarArticulos(Request $data){
        $buscar=$data->get('input');

        $articulos=articulos::orWhere("descripcion","LIKE","%$buscar%")
        ->orWhere("codigo","=","$buscar")
        ->orWhere("clave","=","$buscar")
        ->orWhere("cod_barra","=","$buscar")
        ->get();
        $marcas=marcas::all();
        $provedores=provedores::all();
        return view("forms.articulos",compact('articulos','marcas','provedores'));
    }

    public function updateExistenca(Request $data){
        $id=$data->get('id');
        $cantidad=$data->get('cantidad');
        try {
            for ($y=0; $y<count($id); $y++) { 
            $articulo=articulos::find($id[$y]);
            $cant=$articulo->cantidad+$cantidad[$y];
            $articulo->update([
                "cantidad"=>$cant
            ]);
        }
        $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        

        return json_encode($status);
    }
    public function eliminarArticulo(Request $data){
        try {
            $articulo=articulos::find($data->get('id'));
            $articulo->delete();
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }

        return back();
    }
    public function buscar_para_editar(Request $data){
        $id=$data->get("id");
        $articulo=articulos::find($id);
        return json_encode($articulo);
    }
    public function update_articulo(Request $data){
        $id_p=$data->get("id_p");
        try {
            $articulo=articulos::find($id_p);
            $articulo->update($data->all());
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        return json_encode($status);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $articulos=articulos::create($request->all());
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        
        return json_encode($status);
    }
    public function store_marca(Request $request){
        try {
            $marca=marcas::create($request->all());
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        return json_encode($status);
    }
    public function store_provedor(Request $request){
        try {
            $marca=provedores::create($request->all());
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        return json_encode($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
