<?php

namespace App\Http\Controllers;

use App\Models\salidas;
use App\Models\turnos;
use Illuminate\Http\Request;

class generalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function registrar()
    {
        return view('auth.register');
    }

    public function storeturno(Request $data)
    {
        $turno = turnos::create($data->all());
        return back();
    }

    public function storesalidas(Request $data)
    {
        try {
            $salida = salidas::create($data->all());
            $status = "SE REGISTRO CORRECTAMENTE";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);
    }

    public function cerrarturno(Request $data)
    {
        date_default_timezone_set('America/Mexico_City');
        $id     = $data->get("id");
        $cerrar = turnos::find($id);
        $cerrar->update(
            [
                "cierre"      => $data->get("cierre"),
                "comentario2" => $data->get("comentario2"),
                "fecha_fin"   => now(),
                "status"      => $data->get("status"),
            ]
        );
        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
